from flask import Flask
from flask.ext.mongoengine import MongoEngine

app = Flask(__name__)


app.config["MONGODB_SETTINGS"] = {'DB': "my_tumble_log"}
app.config["SECRET_KEY"] = "KeepThisS3cr3t"

"""
app.config["MONGODB_SETTINGS"] = {'DB': "mytest", "host":'mongodb://mytest:123456@ds053818.mongolab.com:53818/heroku_app19177642'}
app.config['DEBUG'] = True
app.config['CSRF_ENABLED'] = False
app.config['SECRET_KEY'] = 'you-will-never-guess'
"""
db = MongoEngine(app)

runP = True

def register_blueprints(app):
    # Prevents circular imports
    from myapp.views import words
    app.register_blueprint(words)
    from myapp.admin import admin
    app.register_blueprint(admin)

register_blueprints(app)

if __name__ == '__main__':
    app.run()