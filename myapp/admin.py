from flask import Blueprint, request, redirect, render_template, url_for
from flask.views import MethodView

from flask.ext.mongoengine.wtf import model_form

from myapp.auth import requires_auth
from myapp.models import KeyWords, JobPost

from myapp.parser import Start

from StringIO import StringIO
from csv import DictReader, writer, DictWriter
  
from flask import Response

admin = Blueprint('admin', __name__, template_folder='templates')

SITES = {
    "all": "All site",
    "indeed": "indeed.com",
    "simplyhired": "simplyhired.com",
    "flexjobs": "flexjobs.com",
    "virtualvocations": "virtualvocations.com"
}

class ListWord(MethodView):
    """display a list of keywords"""
    decorators = [requires_auth]
    cls = KeyWords

    def get(self):
        words = self.cls.objects.all()
        return render_template('admin/listWord.html', words=words)


class DetailWord(MethodView):
    """display the edit form and add a keyword"""
    decorators = [requires_auth]

    def get_context(self, pk=None):
        form_cls = model_form(KeyWords)
        if pk:
            word = KeyWords.objects.get_or_404(pk=pk)
            if request.method == 'POST':
                form = form_cls(request.form, inital=word._data)
            else:
                form = form_cls(obj=word)
        else:
            word = KeyWords()
            form = form_cls(request.form)

        context = {
            "word": word,
            "form": form,
            "create": pk is None
        }
        return context

    def get(self, pk):
        context = self.get_context(pk)
        return render_template('admin/detailWord.html', **context)

    def post(self, pk):
        context = self.get_context(pk)
        form = context.get('form')
        if form.validate():
            word = context.get('word')
            form.populate_obj(word)
            word.save()
            return redirect(url_for('admin.word'))
        return render_template('admin/detailWord.html', **context)

class DeleteWord(MethodView):
    """deleting a keyword"""
    decorators = [requires_auth]

    def get(self, pk):
        word = KeyWords.objects.get_or_404(pk=pk)
        word.delete()
        return redirect(url_for('admin.word'))

class ListJob(MethodView):
    """display a list of Job Posts"""
    decorators = [requires_auth]

    def get(self, page=0, siteName="all"):
        page = int(page)
        onPage = 15
        paginationLink = 3
        off = onPage*page
        if not siteName == "all":
            jobs = JobPost.objects.all().skip(off).limit(onPage)
            countItems = JobPost.objects(siteName=siteName).count()
            #jobs = JobPost.objects.get_or_404(siteName=siteName).skip(off).limit(onPage)
            #countItems = JobPost.objects.filter_by(siteName=siteName).count()
        else:
            jobs = JobPost.objects.all().skip(off).limit(onPage)
            countItems = JobPost.objects.count()
        count = int(round(countItems/onPage+0.5))
        start = page - paginationLink if page > paginationLink else 0
        stop = page + paginationLink if (page + paginationLink) < count  else count

        return render_template('admin/listJob.html', jobs=jobs, start=start, stop=stop, page=page, countPage=count, countItems=countItems, siteName=siteName, listSite=SITES)

class DetailJob(MethodView):
    """display the edit form and add a Job Posts"""
    decorators = [requires_auth]

    def get_context(self, pk=None):
        form_cls = model_form(JobPost)
        if pk:
            job = JobPost.objects.get_or_404(pk=pk)
            if request.method == 'POST':
                form = form_cls(request.form, inital=job._data)
            else:
                form = form_cls(obj=job)
        else:
            job = JobPost()
            form = form_cls(request.form)

        context = {
            "job": job,
            "form": form,
            "create": pk is None
        }
        return context

    def get(self, pk):
        context = self.get_context(pk)
        return render_template('admin/detailJob.html', **context)

    def post(self, pk):
        context = self.get_context(pk)
        form = context.get('form')
        if form.validate():
            job = context.get('job')
            form.populate_obj(job)
            job.save()
            return redirect(url_for('admin.index'))
        return render_template('admin/detailJob.html', **context)

class DeleteJob(MethodView):
    """deleting a Job Posts"""
    decorators = [requires_auth]

    def get(self, pk):
        job = JobPost.objects.get_or_404(pk=pk)
        job.delete()
        return redirect(url_for('admin.index'))

class AdminParser(MethodView):
    """display control page parser"""
    decorators = [requires_auth]

    def get(self):

        p = Start()
        isRun = p.isRun()

        return render_template('admin/parser.html', isRun=isRun)

class StartParser(MethodView):
    decorators = [requires_auth]

    def get(self):
        p = Start()
        p.run()
        return render_template('admin/start_p.html')

class StopParser(MethodView):
    decorators = [requires_auth]

    def get(self):
        p = Start()
        p.stop()
        return render_template('admin/start_p.html')

class Logout(MethodView):
    decorators = [requires_auth]
    def get(self):
        print request.authorization.username
        request.authorization.username = test

        return redirect(url_for('admin.index'))

class CsvSave(MethodView):
    """exports"""
    decorators = [requires_auth]

    def get(self, url=None):
        response = Response(mimetype='text/csv', status=200)
        keys = ['url_job_site', 'job_title', 'location', 'companyName', 'directUrl']
        writer = DictWriter(response.stream, keys, delimiter=';')
        writer.writerow(dict(zip(keys, keys)))

        jobs = JobPost.objects.all()
        for job in jobs: 
            #print job.job_title
            writer.writerow({'url_job_site': job.url_job_site.encode("utf-8"),
                             'job_title': job.job_title.encode("utf-8"),
                             'location': job.location.encode("utf-8"),
                             'companyName': str(job.companyName).encode("utf-8"),
                             'directUrl': job.directUrl.encode("utf-8")})

        return response


# Register the urls
admin.add_url_rule('/admin/csv/data.csv', view_func=CsvSave.as_view('csv'))

admin.add_url_rule('/admin/word/', view_func=ListWord.as_view('word'))
admin.add_url_rule('/admin/word/create/', defaults={'pk': None}, view_func=DetailWord.as_view('wordcreate'), methods=['GET', 'POST'])
admin.add_url_rule('/admin/word/delete/<pk>/', view_func=DeleteWord.as_view('worddelete'), methods=['GET', 'POST'])
admin.add_url_rule('/admin/word/<pk>/', view_func=DetailWord.as_view('wordedit'), methods=['GET', 'POST'])

admin.add_url_rule('/admin/parser/', view_func=AdminParser.as_view('parser'))
admin.add_url_rule('/admin/parser/start/', view_func=StartParser.as_view('start'))
admin.add_url_rule('/admin/parser/stop/', view_func=StopParser.as_view('stop'))
admin.add_url_rule('/admin/logout/', view_func=Logout.as_view('logout'))

admin.add_url_rule('/admin/', view_func=ListJob.as_view('index'))
admin.add_url_rule('/admin/page/<page>', view_func=ListJob.as_view('p'))
admin.add_url_rule('/admin/page/<page>/by/<siteName>', view_func=ListJob.as_view('byname'))
admin.add_url_rule('/admin/create/', defaults={'pk': None}, view_func=DetailJob.as_view('create'), methods=['GET', 'POST'])
admin.add_url_rule('/admin/delete/<pk>/', view_func=DeleteJob.as_view('delete'), methods=['GET', 'POST'])
admin.add_url_rule('/admin/<pk>/', view_func=DetailJob.as_view('edit'), methods=['GET', 'POST'])