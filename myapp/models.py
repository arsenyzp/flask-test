from flask import url_for
from myapp import db

class KeyWords(db.Document):
	word = db.StringField(max_length=255, required=True)

	def get_absolute_url(self):
		return url_for('word', kwargs={"pk": self.pk})

	def __unicode__(self):
		return self.word

	meta = {
		'allow_inheritance': True,
		'indexes': ['word'],
		'ordering': ['word']
	}

class JobPost(db.Document):
	url_job_site = db.StringField(max_length=1024)
	job_title = db.StringField(max_length=255, required=True)
	location = db.StringField(max_length=255)
	companyName = db.StringField(max_length=255)
	directUrl = db.StringField(max_length=1024)
	siteName = db.StringField(max_length=255)
	datePublish = db.IntField()

	def get_absolute_url(self):
		return url_for('job', kwargs={"pk": self.pk})

	def __unicode__(self):
		return self.job_title

	meta = {
		'indexes': ['job_title'],
		'ordering': ['-pk']
	}