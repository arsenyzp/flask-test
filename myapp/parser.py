#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib, lxml.html, urllib2, time, httplib2
from lxml import etree
from myapp.models import JobPost, KeyWords
from myapp import runP
import multiprocessing

import os
import re
from datetime import datetime, date, time, timedelta

class BaseParser(object):

	def HtmlPage(self, url):
		header = {'User-Agent': 'Mozilla/15.0 (X11; Linux i686; rv:15.0) Gecko/20100101 Firefox/15.0'}
		try:
			req = urllib2.Request(url, None, header)
			response = urllib2.urlopen(req)
		except Exception, e:
			return False
		else:
			return response.read()

	def getContentLocation(self, link):
		h = httplib2.Http(".cache_httplib", disable_ssl_certificate_validation=True)
		h.follow_all_redirects = True
		resp = h.request(link, "GET")[0]
		contentLocation = resp['content-location']
		return contentLocation

	def getKeyWords(self):
		return KeyWords.objects.all()

	def parseSite(self):
		for word in self.getKeyWords():
			self.parsePage(word.word)

	def parsePage(self, keyWord):
		raise NotImplementedError()

	def parseDate(self, strDat):
		strDat = strDat.lower()
		match = re.search( ur"\d{1,2}", strDat )
		item = match.group()
		index = strDat.find(item)+2
		strDat = strDat[index:]
		strDat = strDat.strip()
		listT = strDat.split(' ')
		key = listT[0]
		search = {"hour": 1, "hours": 1, "min": 1, "mins": 1, "day": 2, "days": 2, "month": 3, "year": 4, "years": 4, }
		now_date = date.today()
		if key not in search:
			return now_date
		count = int(item)
		tm = search.get(key, 1)
		if tm == 1:
			return now_date
		if tm == 2:
			delta = timedelta(days=count)
		if tm == 3:
			delta = timedelta(month=count)
		if tm == 4:
			delta = timedelta(years=count)

		return now_date - delta


class ParserIndeed(BaseParser):

	def __init__(self):
		self.base_url = "http://www.indeed.com"

	def parsePage(self, keyWord):
		flag = True
		pageNumber = 1
		nextPageUrl = "/jobs?"+ urllib.urlencode({'q': keyWord})
		while flag and runP:
			print pageNumber
			pageNumber = pageNumber+1
			print self.base_url + nextPageUrl
			page = self.HtmlPage(self.base_url + nextPageUrl)
			if not page:
				return
			dom = lxml.html.document_fromstring(page)
			result = dom.cssselect('td[id=resultsCol] div.row')

			paginLink = dom.cssselect('div.pagination a')
			link = paginLink[len(paginLink)-1].get('href')
			if not paginLink[len(paginLink)-1].cssselect('span.pn span.np'):
				flag = False
			nextPageUrl = link

			for element in result:
				title_block = element.cssselect('h2.jobtitle a')
				if len(title_block) == 0:
					continue
				url_job_site = self.base_url + title_block[0].get('href')
				job_title = title_block[0].text_content()
				#job_title = etree.tostring(title_block[0], encoding='utf8', method="text")
				if job_title is None:
					continue
				location = element.cssselect('span.location span[itemprop=addressLocality]')[0].text
				try:
					companyName = element.cssselect('span.company span[itemprop=name]')[0].text
				except:
					companyName = 'unknown'
				try:
					directUrl = self.getContentLocation(url_job_site)
				except:
					pass
				item = JobPost(url_job_site=url_job_site, job_title=job_title, location=location, companyName=companyName, directUrl=directUrl, siteName="indeed")
				item.save()

class ParserSimplyhired(BaseParser):

	def __init__(self):
		self.base_url = "http://www.simplyhired.com"

	def parsePage(self, keyWord):
		url = "http://www.simplyhired.com"
		flag = True
		pageNumber = 1
		nextPageUrl = "/search?"+urllib.urlencode({'q': keyWord})
		while flag and runP:
			print pageNumber
			pageNumber = pageNumber+1
			print url + nextPageUrl

			page = self.HtmlPage(url + nextPageUrl+"&pn="+str(pageNumber-1))
			if not page:
				return
			dom_s = lxml.html.document_fromstring(page)

			result = dom_s.cssselect('li[class=result]')
			try:
				result[0].text
			except:
				flag = False
				break

			for element in result:
				title_block = element.cssselect('h2 a')
				if len(title_block) == 0:
					continue
				url_job_site = self.base_url + title_block[0].get('href')
				job_title = title_block[0].text_content()
				location = element.cssselect('span.location')[0].text or ''
				try:
					companyName = element.cssselect('h4.company ')[0].text
				except IndexError:
					companyName = 'unknown'
				try:
					directUrl = self.getContentLocation(url_job_site)
				except:
					pass

				publishDateBlock = element.cssselect('span.ago')
				publishStr = publishDateBlock[0].text_content()

				dataPost = publishStr.strip()
				datePublish = self.parseDate(dataPost)
				print datePublish
				dt = int(datePublish.strftime("%s"))
				print dt

				item = JobPost(url_job_site=url_job_site, job_title=job_title, location=location, companyName=companyName, directUrl=directUrl, siteName="simplyhired", datePublish=dt)
				item.save()


class ParserFlexjobs(BaseParser):

	def __init__(self):
		self.base_url = "http://www.flexjobs.com"

	def parseJob(self, url):
		try:
			page = self.HtmlPage(url)
			dom = lxml.html.document_fromstring(page)
			result = dom.cssselect('div[id=main] ul.alternate1 li')
			for element in result:
				test = element.cssselect('b')
				if test[0].text == "Location:":
					#location = etree.tostring(element, method="text")
					location = element.text_content()
					location = location.replace("Location:", "")
					return location
		except:
			pass
		finally:
			return 'unknown'


	def parsePage(self, keyWord):
		flag = True
		pageNumber = 1
		nextPageUrl = "/search?"+urllib.urlencode({'q': keyWord})
		while flag and runP:
			print pageNumber
			pageNumber = pageNumber+1
			print self.base_url + nextPageUrl

			page = self.HtmlPage(self.base_url + nextPageUrl)
			if not page:
				return
			dom = lxml.html.document_fromstring(page)
			paginLink = dom.cssselect('div.pagination a.next_page')
			if not paginLink:
				flag = False
			else:
				nextPageUrl = paginLink[0].get('href')

			
			result = dom.cssselect('div[id=joblistarea] li.row')
			for element in result:
				title_block = element.cssselect('div.job-title a')
				if len(title_block) == 0:
					continue
				url_job_site = self.base_url + title_block[0].get('href')
				job_title = title_block[0].text
				print url_job_site
				if job_title is None:
					continue
				location = self.parseJob(url_job_site)
				companyName = 'unknown'
				directUrl = 'unknown'

				datePublishElement = element.cssselect('li.col3')
				datePublish = datePublishElement[0].text_content()
				match = re.search( ur"\d{2}/{1}\d{2}/{1}\d{2}", datePublish )
				datePublish = match.group()
				dt = int(datetime.strptime(datePublish, '%d/%m/%y').strftime("%s"))
				item = JobPost(url_job_site=url_job_site, job_title=job_title, location=location, companyName=companyName, directUrl=directUrl, siteName="flexjobs", datePublish=dt)
				item.save()


from selenium import webdriver


class ParserVocations(BaseParser):

	def __init__(self):
		self.base_url = "http://www.virtualvocations.com"

	def parsePage(self, keyWord):
		flag = True
		pageNumber = 1
		nextPageUrl = self.base_url +  "/jobs/q-"+keyWord.replace(' ', '+')
		while flag and runP:
			print pageNumber
			pageNumber = pageNumber+1
			print nextPageUrl

			page = self.HtmlPage(nextPageUrl)
			if not page:
				return
			dom = lxml.html.document_fromstring(page)

			paginLink = dom.cssselect('p.pagination a')
			
			if not paginLink[len(paginLink)-1].text == "Next >>" :
				flag = False
			else:
				nextPageUrl = paginLink[len(paginLink)-1].get('href')

			result = dom.cssselect('div[class=job_search_results]')
			for element in result:
				title_block = element.cssselect('h2 a')
				if len(title_block) == 0:
					continue
				url_job_site = title_block[0].get('href')
				job_title = title_block[0].text_content()
				try:
					location = element.cssselect('span.location span[itemprop=addressLocality]')[0].text
				except IndexError:
					location = 'unknown'
				try:
					companyName = element.cssselect('span.company span[itemprop=name]')[0].text
				except IndexError:
					companyName = 'unknown'
				try:
					directUrl = self.getContentLocation(url_job_site)
				except:
					directUrl = 'unknown'

				dataPostBlock = element.cssselect('div.blueCond')
				dataPost = dataPostBlock[0].text_content()
				dataPost = dataPost.strip()
				datePublish = self.parseDate(dataPost)
				print datePublish
				dt = int(datePublish.strftime("%s"))
				print dt
				item = JobPost(url_job_site=url_job_site, job_title=job_title, location=location, companyName=companyName, directUrl=directUrl, siteName="virtualvocations", datePublish=dt)
				item.save()

"""

class ParserVocations(BaseParser):

	def parsePage(self, keyWord):
		driver = webdriver.Firefox()
		url = "http://www.virtualvocations.com/jobs"
		driver.get(url)
		element = driver.find_element_by_id('searchbox_keyword')
		element.send_keys(keyWord)
		driver.find_element_by_xpath("//input[@value='Search']").click()

		while driver.find_element_by_link_text("Next >>"):
			page = driver.page_source
			dom = lxml.html.document_fromstring(page)

			result = dom.cssselect('div[class=job_search_results]')
			for element in result:
				title_block = element.cssselect('h2 a')
				if len(title_block) == 0:
					continue
				url_job_site = url + title_block[0].get('href')
				job_title = title_block[0].text_content()
				try:
					location = element.cssselect('span.location span[itemprop=addressLocality]')[0].text
				except IndexError:
					location = 'unknown'
				try:
					companyName = element.cssselect('span.company span[itemprop=name]')[0].text
				except IndexError:
					companyName = 'unknown'
				try:
					directUrl = self.getContentLocation(url_job_site)
				except:
					directUrl = url

				item = JobPost(url_job_site=url_job_site, job_title=job_title, location=location, companyName=companyName, directUrl=directUrl)
				item.save()

				print '!!!', job_title
			driver.find_element_by_link_text("Next >>").click()
		driver.close()

class ParserVocations(BaseParser):

	def parsePage(self, keyWord):
		driver = webdriver.Firefox()
		url = "http://www.virtualvocations.com/jobs"
		driver.get(url)
		element = driver.find_element_by_id('searchbox_keyword')
		element.send_keys(keyWord)
		driver.find_element_by_xpath("//input[@value='Search']").click()
		page = driver.page_source
		dom = lxml.html.document_fromstring(page)
		result = dom.cssselect('div[class=job_search_results]')
		for element in result:
			title_block = element.cssselect('h2 a')
			if len(title_block) == 0:
				continue
			url_job_site = url + title_block[0].get('href')
			job_title = title_block[0].text_content()
			try:
				location = element.cssselect('span.location span[itemprop=addressLocality]')[0].text
			except IndexError:
				location = 'unknown'
			try:
				companyName = element.cssselect('span.company span[itemprop=name]')[0].text
			except IndexError:
				companyName = 'unknown'
			try:
				directUrl = self.getContentLocation(url_job_site)
			except:
				directUrl = url

			item = JobPost(url_job_site=url_job_site, job_title=job_title, location=location, companyName=companyName, directUrl=directUrl)
			item.save()

			print '!!!', job_title
		driver.close()
"""
class Start(object):

	def clearDb(self):
		JobPost.objects.all().delete()

	def __start(self):
		"""start parallel processes parser"""
		self.clearDb()
		runP = True
		p = ParserIndeed()
		#p.parseSite()
		proc = multiprocessing.Process(target=p.parseSite)
		proc.daemon = True
		proc.start()


		p2 = ParserFlexjobs()
		# p.parseSite()
		proc2 = multiprocessing.Process(target=p2.parseSite)
		proc2.daemon = True
		proc2.start()


		p3 = ParserVocations()
		# p.parseSite()
		proc3 = multiprocessing.Process(target=p3.parseSite)
		proc3.daemon = True
		proc3.start()


		p4 = ParserSimplyhired()
		# p.parseSite()
		proc4 = multiprocessing.Process(target=p4.parseSite)
		proc4.daemon = True
		proc4.start()


	def run(self):
		self.stop()
		self.__start()

	def stop(self):
		#queue = multiprocessing.Queue()
		#queue.put('exit')

		for p in multiprocessing.active_children():
			print p.pid
			os.kill(int(p.pid), signal.SIGTERM)
			#os.kill(int(ps_output), signal.SIGTERM)
			#p.current_process().terminate()

		"""
		for p in multiprocessing.active_children():
			if p._daemonic:
				info('calling terminate() for daemon %s', p.name)
				p._popen.terminate()
			
		for p in multiprocessing.active_children():
			info('calling join() for process %s', p.name)
			p.join()
		"""
		runP = False

	def isRun(self):
		return len(multiprocessing.active_children()) > 0


if __name__ == "__main__":
	JobPost.objects.all().delete()
	p = ParserSimplyhired()
	p.parsePage("virtual")