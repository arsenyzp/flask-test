from flask import Blueprint, request, redirect, render_template, url_for
from flask.views import MethodView
from myapp.models import KeyWords, JobPost

words = Blueprint('words', __name__, template_folder='templates')

class ListViewWord(MethodView):

	def get(self):
		words = KeyWords.objects.all()
		return render_template('words/list.html', words=words)

class DetailViewWord(MethodView):

	def get(self, pk):
		word = KeyWords.objects.get_or_404(pk=pk)
		return render_template('words/detail.html', word=word)

class ListViewJob(MethodView):

	def get(self, page=0):
		page = int(page)
		onPage = 15
		paginationLink = 3
		off = onPage*page
		jobs = JobPost.objects.all().skip(off).limit(onPage)
		count = int(round(JobPost.objects.count()/onPage+0.5))
		start = page - paginationLink if page > paginationLink else 0
		stop = page + paginationLink if (page + paginationLink) < count  else count

		return render_template('job/list.html', jobs=jobs, start=start, stop=stop, page=page)

class DetailViewJob(MethodView):

	def get(self, pk):
		job = JobPost.objects.get_or_404(pk=pk)
		return render_template('job/detail.html', job=job)

words.add_url_rule('/', defaults={'page': 0}, view_func=ListViewJob.as_view('joblist'))
words.add_url_rule('/page/<page>', view_func=ListViewJob.as_view('joblistpage'))
words.add_url_rule('/<pk>/', view_func=DetailViewJob.as_view('jobdetail'))